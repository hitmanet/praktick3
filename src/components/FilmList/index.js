import { Film } from '../Film/index'
import { Api } from '../../services/Api'

export class FilmList {
    constructor(films){
        this.films = films
        this.domFilms = this.renderFilms()
    }

    renderFilms(){
        console.log(this.films)
        return this.films.map(film => {
            const { Poster, Title, Year, imdbID } = film
            console.log(imdbID)
            return `
                <div class="film">
                    <img src = "${Poster !== 'N/A' ? Poster : './noimg.jpg'}" alt = 'No film poster'>
                    <span class="film__title>${Title}</span>
                    <span class="film__year">${Year}</span>
                    <a href="#" id="${imdbID}" class="film__link">Learn more</a>
                </div>
                `
        })
    }

    renderIn(domElement){
        domElement.innerHTML = ''
        for (const film of this.domFilms){
            domElement.innerHTML += film
        }
        document.querySelectorAll('.film__link').forEach(link => {
            const { id } = link
            link.onclick = (e) => this.openFilmInList(e, id)
        })
    }

    async openFilmInList(event, filmId){
        const filmInfo = await Api.getFilmsByQuery(`?i=${filmId}`)
        const film = new Film(filmInfo, filmId)
        film.renderIn(document.querySelector('.movie'))
    }
}