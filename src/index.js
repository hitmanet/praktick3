import "@babel/polyfill"
import { FilmList } from './components/FilmList/index'
import { Api } from './services/Api'

const favorites =JSON.parse(localStorage.getItem('favorite')) || { movies: [] }
const movies = new FilmList(favorites.movies)
movies.renderIn(document.querySelector('.favorite'))
document.querySelector('#search__button').onclick = async() => {
    const searchValue = document.querySelector('#search__input').value
    const films = await Api.getFilmsByQuery(`?s=${searchValue}`)
    if (films.Error){
        document.querySelector('.movies').innerHTML = 'Not found'
    } else {
        new FilmList(films).renderIn(document.querySelector('.movies'))
    }
}